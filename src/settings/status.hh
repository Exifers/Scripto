#ifndef SCRIPTO2_STATUS_HH
#define SCRIPTO2_STATUS_HH

#include <map>

typedef enum { LEXER, PARSER, INTERPRETER, PRETTY_PRINT } level_t;
const std::map<level_t, const int> status_code = {
    {LEXER, 1}, {PARSER, 2}, {INTERPRETER, 3}, {PRETTY_PRINT, 103}};

#endif // SCRIPTO2_STATUS_HH
