#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <regex>
#include <string>
#include <vector>

#include "interpreter/interpreter.hh"
#include "lexer/lexer.hh"
#include "lib/clara/clara.hh"
#include "lib/util/input.hh"
#include "parser/parser.hh"
#include "settings/status.hh"
#include "visitor/visitor.hh"

/**
 * @name options
 * @details General program options.
 */
typedef struct options {
  options() : pretty_print(false), show_help(false) {}
  bool pretty_print;
  bool show_help;
  std::string filename;
} options;

/**
 * @name Main
 * @details A class providing entry point for source code.
 */
class Main {
public:
  /**
    * @name run
    * @details The entry point of the source code, performs all interpreting.
    * @param source the source code
    * @param options general program options
    */
  static int run(std::string source, options options);
  static options parse_args(int argc, char **argv);

private:
  /**
   * @name job
   * @details Performs a task defined by callback and throws matching status
   * code integer from level if an error occurred.
   * @param level some task enum mapped to a return status failure
   * @param callback any lambda to be called without parameter
   * @return the return value of the callback
   */
  template <typename callback_t>
  static decltype(auto) job(level_t level, callback_t callback);
};

template <typename func_t>
decltype(auto) Main::job(level_t level, func_t callback) {
  try {
    return callback();
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    throw status_code.at(level);
  }
}

int Main::run(std::string source, options options) {
  try {
    auto tokens = Main::job(LEXER, [source]() {
      auto lexer = Lexer();
      return lexer.scan(source);
    });

    auto ast = Main::job(PARSER, [tokens]() {
      auto parser = Parser(tokens);
      return parser.parse();
    });

    if (options.pretty_print) {
      Main::job(PRETTY_PRINT, [ast]() {
        auto printer = Printer();
        ast->accept(printer);
        std::cout << std::endl;
      });
    }

    Main::job(INTERPRETER, [ast]() {
      auto interpreter = Interpreter();
      ast->accept(interpreter);
    });
  } catch (int status_code) {
    return status_code;
  }

  return 0;
}

options Main::parse_args(int argc, char **argv) {
  options options;

  auto cli = clara::Arg(options.filename, "filename") |
             clara::Opt(options.pretty_print)["-p"]["--pretty-print"](
                 "use AST pretty-printer") |
             clara::Opt(options.filename, "filename")["-f"]["--filename"](
                 "filename of the source to read") |
             clara::Help(options.show_help);

  auto result = cli.parse(clara::Args(argc, argv));

  if (!result) {
    std::cerr << result.errorMessage() << std::endl;
    exit(-1);
  }

  if (options.show_help) {
    std::cout << cli;
    std::exit(0);
  }

  return options;
}

/**
 * @name main
 * @details main function. Reads code from standard input or given file and runs
 * interpreter.
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
  options options = Main::parse_args(argc, argv);

  std::string source;
  if (options.filename != "") {
    source = read_file(options.filename);
  } else {
    std::cout << "Scripto V0.2" << std::endl;
    source = read_cin();
  }

  return Main::run(source, options);
}
