#include <iostream>
#include <stdexcept>

#include "visitor.hh"

/* Visitor (does nothing) */

void Visitor::visit(Node &node) {
  (void)node;
  throw std::invalid_argument("Node instance must have final dynamic type");
}

void Visitor::visit(Exps &exps) {
  for (auto it = exps.get_exps().begin(); it != exps.get_exps().end(); it++) {
    (*it)->accept(*this);
  }
}

void Visitor::visit(Exp &exp) {
  (void)exp;
  throw std::invalid_argument("Exp instance must have final dynamic type");
}

void Visitor::visit(Value &value) { (void)value; }

void Visitor::visit(PrintExp &printExp) { printExp.get_value()->accept(*this); }

void Visitor::visit(AssignExp &assignExp) {
  assignExp.get_lhs()->accept(*this);
  assignExp.get_rhs()->accept(*this);
}

void Visitor::visit(FunctionDec &functionDec) {
  functionDec.get_exps()->accept(*this);
}

void Visitor::visit(FunctionCall &functionCall) { (void)functionCall; }

void Visitor::visit(IfStmt &ifStmt) {
  ifStmt.get_condition()->accept(*this);
  ifStmt.get_exps()->accept(*this);
  if (ifStmt.getElse_exps()) {
    ifStmt.getElse_exps()->accept(*this);
  }
}

void Visitor::visit(Condition &condition) {
  condition.get_lhs()->accept(*this);
  condition.get_op()->accept(*this);
  condition.get_rhs()->accept(*this);
}

void Visitor::visit(Operator &operator_) {}

void Visitor::visit(RepeatStmt &repeatStmt) {
  repeatStmt.get_num()->accept(*this);
  repeatStmt.get_exps()->accept(*this);
}

/* Printer */

void Printer::visit(Exps &exps) {
  for (auto it = exps.get_exps().begin(); it != exps.get_exps().end(); it++) {
    (*it)->accept(*this);
    if (std::next(it) != exps.get_exps().end())
      std::cout << " ";
  }
}

void Printer::visit(Value &value) {
  switch (value.get_memory()) {
  case Value::LVALUE:
    std::cout << value.get_name();
    break;
  case Value::RVALUE:
    switch (value.get_type()) {
    case Value::INT:
      std::cout << value.get_int_value();
      break;
    case Value::STRING:
      std::cout << value.get_string_value();
      break;
    }
    break;
  }
}

void Printer::visit(PrintExp &printExp) {
  std::cout << "print ";
  printExp.get_value()->accept(*this);
}

void Printer::visit(AssignExp &assignExp) {
  assignExp.get_lhs()->accept(*this);
  std::cout << " = ";
  assignExp.get_rhs()->accept(*this);
}

void Printer::visit(FunctionDec &functionDec) {
  std::cout << "function ";
  std::cout << functionDec.get_name();
  std::cout << " { ";
  functionDec.get_exps()->accept(*this);
  std::cout << " }";
}

void Printer::visit(FunctionCall &functionCall) {
  std::cout << functionCall.get_name();
  std::cout << " ( )";
}

void Printer::visit(IfStmt &ifStmt) {
  std::cout << "if ( ";
  ifStmt.get_condition()->accept(*this);
  std::cout << " ) { ";
  ifStmt.get_exps()->accept(*this);
  std::cout << " }";
  auto else_exps = ifStmt.getElse_exps();
  if (else_exps) {
    std::cout << " else { ";
    else_exps->accept(*this);
    std::cout << " }";
  }
}

void Printer::visit(Condition &condition) {
  condition.get_lhs()->accept(*this);
  std::cout << " ";
  condition.get_op()->accept(*this);
  std::cout << " ";
  condition.get_rhs()->accept(*this);
}

void Printer::visit(Operator &operator_) {
  switch (operator_.get_token()) {
  case Token::DEQ:
    std::cout << "==";
    break;
  case Token::LT:
    std::cout << "<";
    break;
  case Token::LTE:
    std::cout << "<=";
    break;
  case Token::GT:
    std::cout << ">";
    break;
  case Token::GTE:
    std::cout << ">=";
    break;
  default:
    throw std::invalid_argument(
        std::string("Internal error: operator node has unexpected token type"));
  }
}

void Printer::visit(RepeatStmt &repeatStmt) {
  std::cout << "repeat (";
  repeatStmt.get_num()->accept(*this);
  std::cout << ") { ";
  repeatStmt.get_exps()->accept(*this);
  std::cout << " }";
}
