#include <iostream>
#include <stdexcept>

#include "interpreter.hh"

/* values */

void Interpreter::resolve_value(std::shared_ptr<Value> value) {
  if (value->get_memory() == Value::LVALUE) {
    if (int_values_.find(value->get_name()) != int_values_.end()) {
      value->get_int_value() = int_values_[value->get_name()];
      value->get_type() = Value::INT;
      return;
    } else if (string_values_.find(value->get_name()) != string_values_.end()) {
      value->get_string_value() = string_values_[value->get_name()];
      value->get_type() = Value::STRING;
      return;
    } else {
      throw std::invalid_argument("Unknown variable: " + value->get_name());
    }
  }
}

void Interpreter::set_int_value(std::string &name, int value) {
  int_values_[name] = value;
  if (string_values_.find(name) != string_values_.end()) {
    string_values_.erase(string_values_.find(name));
  }
}

void Interpreter::set_string_value(std::string &name, std::string &value) {
  string_values_[name] = value;
  if (int_values_.find(name) != int_values_.end()) {
    int_values_.erase(int_values_.find(name));
  }
}

/* functions */

void Interpreter::set_function(std::string &name,
                               std::shared_ptr<Node> function) {
  functions_[name] = function;
}

std::shared_ptr<Node> Interpreter::read_function(FunctionCall &functionCall) {
  if (functions_.find(functionCall.get_name()) == functions_.end()) {
    throw std::invalid_argument("Unknown function: " + functionCall.get_name());
  }
  return functions_[functionCall.get_name()];
}

/* conditions */

bool Interpreter::eval_condition(Condition &condition) {
  if (condition.get_lhs()->get_type() != condition.get_rhs()->get_type()) {
    throw std::invalid_argument(
        "Cannot compare values of type " +
        std::to_string(condition.get_lhs()->get_type()) + " and " +
        std::to_string(condition.get_rhs()->get_type()));
  }
  switch (condition.get_lhs()->get_type()) {
  case Value::INT:
    return eval_int_condition(condition);
  case Value::STRING:
    return eval_string_condition(condition);
  default:
    throw std::invalid_argument(
        std::string("Internal error: value node has an unexpected token type"));
  }
}

bool Interpreter::eval_int_condition(Condition &condition) {
  switch (condition.get_op()->get_token()) {
  case Token::DEQ:
    return condition.get_lhs()->get_int_value() ==
           condition.get_rhs()->get_int_value();
  case Token::NE:
    return condition.get_lhs()->get_int_value() !=
           condition.get_rhs()->get_int_value();
  case Token::LT:
    return condition.get_lhs()->get_int_value() <
           condition.get_rhs()->get_int_value();
  case Token::LTE:
    return condition.get_lhs()->get_int_value() <=
           condition.get_rhs()->get_int_value();
  case Token::GT:
    return condition.get_lhs()->get_int_value() >
           condition.get_rhs()->get_int_value();
  case Token::GTE:
    return condition.get_lhs()->get_int_value() >=
           condition.get_rhs()->get_int_value();
  default:
    throw std::invalid_argument(
        std::string("Internal error: operator node has an unexpected token"));
  }
}

bool Interpreter::eval_string_condition(Condition &condition) {
  switch (condition.get_op()->get_token()) {
  case Token::DEQ:
    return condition.get_lhs()->get_string_value().compare(
               condition.get_rhs()->get_string_value()) == 0;
  case Token::NE:
    return condition.get_lhs()->get_string_value().compare(
               condition.get_rhs()->get_string_value()) != 0;
  case Token::LT:
    return condition.get_lhs()->get_string_value().compare(
               condition.get_rhs()->get_string_value()) > 0;
  case Token::LTE:
    return condition.get_lhs()->get_string_value().compare(
               condition.get_rhs()->get_string_value()) >= 0;
  case Token::GT:
    return condition.get_lhs()->get_string_value().compare(
               condition.get_rhs()->get_string_value()) < 0;
  case Token::GTE:
    return condition.get_lhs()->get_string_value().compare(
               condition.get_rhs()->get_string_value()) <= 0;
  default:
    throw std::invalid_argument(
        std::string("Internal error: operator node has unexpected token type"));
  }
}

/* visit methods */

void Interpreter::visit(PrintExp &printExp) {
  resolve_value(printExp.get_value());
  switch (printExp.get_value()->get_type()) {
  case Value::INT:
    std::cout << printExp.get_value()->get_int_value() << std::endl;
    break;
  case Value::STRING:
    auto str = printExp.get_value()->get_string_value();
    auto sub_str = str.substr(1, str.size() - 2);
    std::cout << sub_str << std::endl;
    break;
  }
}

void Interpreter::visit(AssignExp &assignExp) {
  resolve_value((assignExp.get_rhs()));
  switch (assignExp.get_rhs()->get_type()) {
  case Value::INT:
    set_int_value(assignExp.get_lhs()->get_name(),
                  assignExp.get_rhs()->get_int_value());
    break;
  case Value::STRING:
    set_string_value(assignExp.get_lhs()->get_name(),
                     assignExp.get_rhs()->get_string_value());
    break;
  }
}

void Interpreter::visit(FunctionDec &functionDec) {
  set_function(functionDec.get_name(), functionDec.get_exps());
}

void Interpreter::visit(FunctionCall &functionCall) {
  auto function = read_function(functionCall);
  function->accept(*this);
}

void Interpreter::visit(IfStmt &ifStmt) {
  resolve_value(ifStmt.get_condition()->get_lhs());
  resolve_value(ifStmt.get_condition()->get_rhs());

  if (eval_condition(*ifStmt.get_condition())) {
    ifStmt.get_exps()->accept(*this);
  } else if (ifStmt.getElse_exps() != nullptr) {
    ifStmt.getElse_exps()->accept(*this);
  }
}

void Interpreter::visit(RepeatStmt &repeatStmt) {
  std::string e("e");
  for (int i = 0; i < repeatStmt.get_num()->get_int_value(); i++) {
    set_int_value(e, i);
    repeatStmt.get_exps()->accept(*this);
  }
}
