# Scripto2
Small test script langage, v0.2.

## Features
- [X] Lexer
- [X] Parser
- [X] Pretty-printer
- [X] Interpreter

## Prerequisites
- GNU/Linux
- g++
- make

## Compile
`make`

## Use
`./main`
Enter some code and type enter it will show the AST pretty-printed.
Type `Ctrl+D` to exit.

## Grammar
Check the wiki.

## Usage
`./scripto2 <filename>`
or
`./scripto2`, then enter code, then `Ctrl+D`

## Sheebang
Scripto2 accepts an eventual '#' on the first line of the file, if it finds it, it skips the first line.

# Authors
Exifers,
July 2018
