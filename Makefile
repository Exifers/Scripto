CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++17 -I .

bin = main
fls = $(shell find src/ lib/ -type f -name "*.cc")
obj = $(addsuffix .o, $(basename $(fls)))

debug : CXXFLAGS += -g

all: $(obj)
	$(CXX) $(CXXFLAGS) $(obj) -o $(bin)

debug: $(obj)
	$(CXX) $(CXXFLAGS) $(obj) -o $(bin)

format:
	./scripts/format/format_diff.sh

format-all:
	./scripts/format/format.sh

test:
	./test/test.sh

clean:
	$(RM) $(bin) $(obj)

.PHONY: all debug format format-all test clean
