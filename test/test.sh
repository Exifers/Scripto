#! /bin/bash

source ./test/utils/utils.sh

e_header "SCRIPTO2 TEST-SUITE"

echo -n "1. SOURCE FORMAT "
./test/test_format.sh && e_success "ok" || exit -1

echo -n "2. MEMORY LEAK "
v_out=$(valgrind 2>&1 ./test/test.o2)
echo "$v_out" | grep -e "definitely lost: 0 bytes in 0 blocks" > /dev/null
test $? -eq 0 || exit -1
echo "$v_out" | grep -e "indirectly lost: 0 bytes in 0 blocks" > /dev/null
test $? -eq 0 || exit -1
echo "$v_out" | grep -e "possibly lost: 0 bytes in 0 blocks" > /dev/null
test $? -eq 0 || exit -1
e_success "ok"

echo -n "3. AST/OUTPUT "
diff <(./test/test.o2) ./test/test.expected && e_success "ok" || exit -1
