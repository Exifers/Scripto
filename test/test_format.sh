#! /bin/bash

src=$(find src/ lib/ -type f -name '*.cc' -or -name '*.hh')

for file in $src; do
  formatted=$(./clang-format $file --style=llvm; echo -n "x")
  difference=$(diff <(echo -n "$formatted") <(cat $file; echo -n "x"))
  if [ ! -z "$difference" ]
  then
    echo "$file"
    echo "$difference"
    exit -1
  fi
done

exit 0
