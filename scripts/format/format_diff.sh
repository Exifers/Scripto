#! /bin/sh

git diff -U0 --no-color | ./scripts/format/clang-format-diff.py -p1 -i -binary './clang-format'
